﻿#region snippet_MainWindowClass
using System;
using System.Threading.Tasks;
using System.Windows;
using Microsoft.AspNetCore.SignalR.Client;
using Microsoft.Extensions.Logging;

namespace SignalRChatClient
{
    public partial class MainWindow : Window
    {
        HubConnection connection;
        public MainWindow()
        {
            InitializeComponent();

            connection = new HubConnectionBuilder()
                .WithUrl("https://localhost:44357/BetActivityHub")
                .ConfigureLogging(logging =>
                {
                    // logging.AddDebug;

                    // This will set ALL logging to Debug level
                    logging.SetMinimumLevel(LogLevel.Debug);
                })
                // TODO: Auto reconnect here
                .Build();

            connection.Closed += async (error) =>
            {
                await Task.Delay(new Random().Next(0,5) * 1000);
                await connection.StartAsync();
            };
            
            // Subscribe to Method BetTickerMessage
            connection.On<SignalrTickerPayload>("BetTickerMessage", (payload) =>
            {
                this.Dispatcher.Invoke(() =>
                {
                    var newMessage = $"{payload.ConnectionId}: {payload.FilterDataId}";
                    messagesList.Items.Add(newMessage);
                });
            });

            try
            {
                connection.StartAsync();
                messagesList.Items.Add("Connection started");
            }
            catch (Exception ex)
            {
                messagesList.Items.Add(ex.Message);
            }
            
            sendButton.IsEnabled = true;
            
            
        }

        // On new TickerBet received
        private async void connectButton_Click(object sender, RoutedEventArgs e)
        {
            
            connection.On<SignalrTickerPayload>("BetTickerMessage", (payload) =>
            {
                this.Dispatcher.Invoke(() =>
                {
                   var newMessage = $"{payload.ConnectionId}: {payload.FilterDataId}";
                   messagesList.Items.Add(newMessage);
                });
            });
            
            
            
            
            

            try
            {
                await connection.StartAsync();
                messagesList.Items.Add("Connection started");
                // connectButton.IsEnabled = false;
                // sendButton.IsEnabled = true;
            }
            catch (Exception ex)
            {
                messagesList.Items.Add(ex.Message);
            }
        }

        private async void sendButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                #region snippet_InvokeAsync
                await connection.InvokeAsync("SendToSubscribers", 
                    new SignalrTickerPayload(){FilterDataId= new Guid(),ConnectionId="12345",TickerBet= new TickerBet()});
                #endregion
            }
            catch (Exception ex)
            {                
                messagesList.Items.Add(ex.Message);                
            }
        }
    }
   
    
    
    public class SignalrTickerPayload
    {
        public Guid FilterDataId { get; set; }
        public string ConnectionId { get; set; }
        public TickerBet TickerBet { get; set; }
    }

    public class TickerBet
    {
    }
}
#endregion
